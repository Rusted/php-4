<?php

$colors = [
    'BlanchedAlmond',
    'CadetBlue',
    'BurlyWood',
    'favorite' => 'DarkOliveGreen',
    'HotPink',
    'PapayaWhip',
    'Magenta',
    '#BADA55',
];
?>

<table border="1">
    <tr>
        <th>Key</th>
        <th>Color</th>
    </tr>

    <?php foreach ($colors as $key => $color) {?>
    <tr style="background-color:<?php echo $color; ?>">
        <td><?php echo $key; ?></td>
        <td><?php echo $color; ?></td>
    </tr>
    <?php }?>
</table>